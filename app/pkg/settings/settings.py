from functools import lru_cache
from pathlib import Path

from dotenv import find_dotenv
from pydantic.class_validators import validator
from pydantic.env_settings import BaseSettings
from pydantic.types import SecretStr

__all__ = ["Settings", "get_settings"]


class _Settings(BaseSettings):
    class Config:
        """Configuration of settings."""

        #: str: env file encoding.
        env_file_encoding = "utf-8"
        #: str: allow custom fields in model.
        arbitrary_types_allowed = True


class Settings(_Settings):
    """Server settings.

    Formed from `.env` or `.env.dev`.
    """

    STATIC_PATH: Path

    #: SecretStr: secret x-token for authority.
    X_API_TOKEN_STATIC: SecretStr

    @validator("STATIC_PATH")
    def create_static_path(cls, v: Path) -> Path:
        """Create static files directory."""
        if not v.exists():
            v.mkdir(parents=True, exist_ok=True)
        return v


@lru_cache()
def get_settings(env_file: str = ".env") -> Settings:
    """Create settings instance."""
    return Settings(_env_file=find_dotenv(env_file))
