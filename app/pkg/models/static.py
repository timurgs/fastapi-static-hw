from pathlib import Path

from pydantic import BaseModel


class StaticDist(BaseModel):
    image_path: Path
