"""Base business models.

All models *must* be inherited by them.
"""

from .exception import BaseAPIException
