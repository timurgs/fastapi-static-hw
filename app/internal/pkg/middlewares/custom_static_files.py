import aiofiles
import aiohttp

from starlette.staticfiles import StaticFiles
from fastapi import Request
from app.internal.pkg.middlewares.x_auth_token import verify_x_auth_token

__all__ = [
    "CustomStaticFiles",
]


async def load_image():
    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.thecatapi.com/v1/images/search') as response:
            data = await response.json()
            url = data[0]["url"]
            async with session.get(url) as response_2:
                async with aiofiles.open(f'./static/cat.jpg', 'wb') as file:
                    await file.write(await response_2.read())
                    return


class CustomStaticFiles(StaticFiles):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    async def __call__(self, scope, receive, send) -> None:
        assert scope["type"] == "http"

        request = Request(scope, receive)
        await verify_x_auth_token(request)

        if request.url.path == "/images_static/cat.jpg":
            await load_image()

        await super().__call__(scope, receive, send)
