from fastapi import Depends, FastAPI

from app.internal.pkg.middlewares.x_auth_token import get_x_token_key
from app.internal.pkg.middlewares.custom_static_files import CustomStaticFiles
from app.pkg.settings import settings

app = FastAPI(dependencies=[Depends(get_x_token_key)])
app.mount(
    "/images_static",
    CustomStaticFiles(directory=settings.STATIC_PATH),
    name="static",
)
